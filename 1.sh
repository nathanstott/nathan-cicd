echo "RewriteEngine on"
echo "RewriteCond %{HTTP_REFERER} !^$"
echo "RewriteCond %{HTTP_REFERER} !^http://(www\.)webleash.co.uk/.*$ [NC]"
echo "RewriteRule \.(gif|jpg|jpeg|bmp|zip|rar|mp3|flv|swf|xml|php|png|css|pdf)$ - [F]"

