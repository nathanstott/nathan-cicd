# Build Dockerfile
sudo docker build -t webv1 .

# Docker run the container
sudo docker run -dit --name my-running-webv1 -p 8081:80 webv1

# Tag the container

sudo docker tag f596b71f085a eu.gcr.io/mynetworkcloudguru/webleash:ver1

# Push the container. 

sudo docker push eu.gcr.io/mynetworkcloudguru/webleash:ver1


